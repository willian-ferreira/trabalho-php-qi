<?php

session_start();
session_unset();
include '../Model/ManipulaEmpresa.php';
include '../Model/Empresa.php';

$op = $_GET['op'];
switch ($op) {
    case 'cadastrar':
        $f = new Empresa();
        $f->nome = $_POST['nome'];
        $f->login = $_POST['login'];
        $f->senha = $_POST['senha'];
        $f->cidade = $_POST['cidade'];
        $me = new ManipulaEmpresa();
        $me->cadastrarEmpresa($f);
        $_SESSION['nome'] = serialize($f);
        header("location:../View/RespostaEmpresa.php");
        break;
    default:
        echo "Op Inválida";
    case 'buscar':
        $me = new ManipulaEmpresa();
        $novoArray = $me->BuscarEmpresa();
        $_SESSION['dados'] = serialize($novoArray);
        header('location:../View/Editar.php');
        break;

    case 'filtrar':
        if ($_POST['filtro'] == "login") {
            $query = "where login like '%" . $_POST['valor'] . "%'";
        } else if ($_POST['filtro'] == "cidade") {
            $query = "where cidade='" . $_POST['valor'] . "'";
        } else if ($_POST['filtro'] == "idfilial") {
            $query = "where idfilial='" . $_POST['valor'] . "'";
        } else {
            $query = "";
        }
        $me = new ManipulaEmpresa();
        $dados = $me->filtrarEmpresa($query);
        $_SESSION['dados'] = serialize($dados);
        header('location:../View/Editar.php');
        break;
    case 'deletar':
        $me = new ManipulaEmpresa();
        echo $_POST['idfilial'];
        $me->deletarEmpresa($_POST['idfilial']);
        header('location:ControleEmpresa.php?op=buscar');
        break;
    case 'alterar':
        $f = new Empresa();
        $f->nome = $_POST['nome'];
        $f->login = $_POST['login'];
        $f->senha = $_POST['senha'];
        $f->cidade = $_POST['cidade'];
        $me = new ManipulaEmpresa();
        $me->alterarEmpresa($f);
        $_SESSION['nome'] = serialize($f);

    default: echo "Op Inválida";
}

