<?php

class Carro {
    private $idcarro;
    private $modelo;
    private $marca;
    private $placa;
    private $filial;
    private $preco;

    public function __get($name) {
        return $this->$name;
    }

    public function __set($name, $value) {
        $this->$name=$value;
    }

    public function __toString() {
        return "<h1>idcarro: ".$this->idcarro."</h1>".
                "<br/>Modelo: ".$this->modelo.
                "<br/>Marca: ".$this->marca.
                "<br/>Placa: ".$this->placa.
                "<br/>Empresa: ".$this->filial.
                "<br/>Preco: ".$this->preco;

    }
}
