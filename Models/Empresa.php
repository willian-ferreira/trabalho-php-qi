<?php

class Empresa {
    private $idfilial;
    private $nome;
    private $login;
    private $senha;
    private $cidade;

       public function __get($name) {
        return $this->$name;
    }

    public function __set($name, $value) {
        $this->$name=$value;
    }

    public function __toString() {
        return "<h1>Filial: ".$this->idfilial."</h1>".
                "<br/>Nome: ".$this->nome.
                "<br/>Login: ".$this->login.
                "<br/>Senha: ".$this->senha.
                "<br/>Cidade: ".$this->cidade;

    }
}
