<?php

include 'Conexao.php';

class UsuarioModel {

    private $conexao = null;

    function __construct() {

        $this->conexao = Conexao::getInstancia();
    }

    function cadastrarUsuario($u) {
        try {
            $sql = $this->conexao->prepare("insert into cliente"
                    . "(idcliente,nome,login,senha,tel,cpf)"
                    . " values(null,?,?,?,?,?)");
            $sql->bindValue(1, $u->nome);
            $sql->bindValue(2, $u->login);
            $sql->bindValue(3, $u->senha);
            $sql->bindValue(4, $u->tel);
            $sql->bindValue(5, $u->cpf);
            $sql->execute();
            $this->conexao = null;
        } catch (Exception $e) {
            echo "Erro ao Inserir!";
        }
    }

    function FiltrarUsuario($query) {
        try {


            $sql = $this->conexao->query('select '
                    . '* from cliente ' . $query);
            $array = array();

            $array = $sql->fetchAll(PDO::FETCH_CLASS, "Usuario");
            $this->conexao = null;
            return $array;
        } catch (Exception $e) {
            echo "erro ao buscar!";
        }
    }

    function FiltrarAdm($query) {
        try {


            $sql = $this->conexao->query('select '
                    . '* from usuarioadmin ' . $query);

            $array = array();
            $array = $sql->fetchAll(PDO::FETCH_CLASS, "UsuarioADM");
            $this->conexao = null;
            return $array;
        } catch (Exception $e) {
            echo "erro ao buscar!";
        }
    }

}
